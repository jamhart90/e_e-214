#button
set_property -dict { PACKAGE_PIN W14   IOSTANDARD LVCMOS33 } [get_ports { button }];
set_property -dict { PACKAGE_PIN W13   IOSTANDARD LVCMOS33 } [get_ports { rst }];


#clock
set_property -dict { PACKAGE_PIN H16  IOSTANDARD LVCMOS33 } [get_ports { clk }];

#led
set_property -dict { PACKAGE_PIN N20  IOSTANDARD LVCMOS33 } [get_ports { q }];
