`timescale 1ns / 1ps

module DebounceStateMachine(
    input button,
    input rst,
    input clk,
    output reg q
    );
    localparam S0 = 2'b00;
    localparam S1 = 2'b01;
    localparam S2 = 2'b10;
    localparam S3 = 2'b11;
    
    reg [1:0] pState, nState;
    
     always@(pState,button) begin
     case(pState)
        S0 : nState = button ? S1 : S0; // move to State 1 if button is 1 else stay at State 0
        S1 : nState = button ? S2 : S0; // move to State 2 if button is 1 else go to State 0
        S2 : nState = button ? S2 : S3; // move to State 3 if button is 0 else stay at State 2
        S3 : nState = button ? S2 : S0; // move to State 0 if button is 0 else go to State 2
        default : nState = S0;
    endcase
    end
    
    always@(posedge(clk),posedge(rst)) begin
    pState <= (rst) ? S0 : nState;
    q <= (pState == S0) ? 0 :
         (pState == S1) ? 1 :
         (pState == S2) ? 1 : 0;
    end
        
endmodule
