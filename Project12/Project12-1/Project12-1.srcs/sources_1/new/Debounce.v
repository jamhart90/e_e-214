`timescale 1ns / 1ps

module clockDiv(
    input clk_in,
    output reg clk_out
    );
    reg [25:0] count = 0;
    
    always @(posedge clk_in)
    begin
    count<=count+1;
    if (count== 12500)
    begin
        count<=0;
        clk_out=~clk_out;
    end
    end
endmodule

module Debounce(
    input button,
    input clk,
    input rst,
    output q
    );
    
    wire clk_int;
    
    clockDiv(clk,clk_int);
    DebounceStateMachine(button,rst,clk_int,q);
endmodule
