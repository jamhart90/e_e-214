`timescale 1ns / 1ps

module FullAdder(
    input A,
    input B,
    input Cin,
    output reg S
    );
    
    always@* begin
    S =  A ^ B ^ Cin;
    end
endmodule
