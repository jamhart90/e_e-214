`timescale 1ns / 1ps

module wrapper(
    input [7:0] SW,
    input LE,
    input Reset,
    input Sub,
    input clk,
    output Cout,
    output [7:0] Cathodes,
    output [3:0] Anodes
    );
    wire [7:0] A;
    wire [7:0] B;
    wire [8:0] S;

    // ADD/SUB B Input
    PIPORegister BReg(.DataIn(SW),.LoadEnable(LE),.Rst(Reset),.DataOut(B));
    // ADD/SUB A Input
    generate
    genvar i;
    for (i=0;i<8;i=i+1)
    begin : XOR
        assign A[i] = SW[i]^Sub;
        end
    endgenerate
    
    
    ADDER8BIT Adder(.A(A),.B(B),.Cin(Sub),.S(S));
    
    SSDWrapper DisplayControl(.binary(S[7:0]),.CLK(clk),.Cathodes(Cathodes),.Anodes(Anodes));
    
    assign Cout = S[8];
endmodule
