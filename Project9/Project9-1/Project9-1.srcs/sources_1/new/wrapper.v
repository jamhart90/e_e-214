`timescale 1ns / 1ps

module wrapper(
    input [7:0] SW,
    input Cin,
    input clk,
    output [7:0] Cathodes,
    output [3:0] Anodes
    );
    wire [7:0] binary;
    
    CarryLookAheadAdder CLA(.A(SW[3:0]),.B(SW[7:4]),.Cin(Cin),.S(binary),.Cout(Cout));
    SSDWrapper SSD(.binary(binary),.CLK(clk),.Cathodes(Cathodes),.Anodes(Anodes));
    
endmodule
