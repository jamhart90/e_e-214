`timescale 1ns / 1ps

module wrapper(
    input [7:0] SW,
    input [2:0] LE,
    input Reset,
    input clk,
    output [7:0] Cathodes,
    output [3:0] Anodes,
    output [2:0] COMPLED
    );
    wire [7:0] A;
    wire [7:0] B;
    wire [15:0] S;
    wire [15:0] MultRegOut;
    
    PIPORegister AReg(.DataIn(SW),.LoadEnable(LE[0]),.Rst(Reset),.DataOut(A));
    PIPORegister BReg(.DataIn(SW),.LoadEnable(LE[1]),.Rst(Reset),.DataOut(B));
    
    
    MULT8BIT Multiplier(.A(A),.B(B),.X(S));
    
    SSDWrapper DisplayControl(.binary(S),.CLK(clk),.Cathodes(Cathodes),.Anodes(Anodes));
    
    PIPORegister #(.WIDTH(16))(.DataIn(S),.LoadEnable(LE[2]),.Rst(Reset),.DataOut(MultRegOut));
    
    Comparator Comp(.A(S),.B(MultRegOut),.LT(COMPLED[0]),.EQ(COMPLED[1]),.GT(COMPLED[2]));
endmodule
