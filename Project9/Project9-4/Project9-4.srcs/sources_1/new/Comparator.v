`timescale 1ns / 1ps

module Comparator(
    input [15:0] A,
    input [15:0]B,
    output LT,
    output EQ,
    output GT
    );
    assign LT = (A<B);
    assign EQ = (A==B);
    assign GT = (A>B);
endmodule
