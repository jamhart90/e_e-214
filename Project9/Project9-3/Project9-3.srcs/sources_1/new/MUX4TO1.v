`timescale 1ns / 1ps

module MUX4TO1(
    input [3:0] IN0,
    input [3:0] IN1,
    input [3:0] IN2,
    input [3:0] IN3,
    input [1:0] sel,
    output reg [3:0] OUT
    );
   always@*
   case (sel)
        2'b00: OUT = IN0;
        2'b01: OUT = IN1;
        2'b10: OUT = IN2;
        2'b11: OUT = IN3;
    endcase
endmodule
