`timescale 1ns / 1ps

module ADDER8BIT(
input [7:0] A,
input [7:0] B,
input Cin,
output reg [8:0] S
    );
    reg [7:0] A_int;
    
    always@* begin
    A_int = A + {{7{1'b0}},Cin};
    S = B + A_int;
    end
endmodule
