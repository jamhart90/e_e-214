`timescale 1ns / 1ps
module DFF(
    input D,
    input LoadEnable,
    input RST,
    output reg Q
    );
    always @(posedge LoadEnable,posedge RST)
    begin
    if(RST)
        Q <= 1'b0;
    else
        Q <= D;
    end
    
    
endmodule

module PIPORegister(
    input [7:0] DataIn,
    input LoadEnable,
	input Rst,
    output [7:0] DataOut
    );
    generate
    genvar i;
    for(i = 0 ; i < 8 ; i = i+1) begin : DFF
        DFF DFF(.D(DataIn[i]),.LoadEnable(LoadEnable),.RST(Rst),.Q(DataOut[i]));
        end
    endgenerate
       
endmodule
