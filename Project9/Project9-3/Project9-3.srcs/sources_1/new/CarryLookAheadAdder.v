`timescale 1ns / 1ps

module CarryLookAheadAdder(
    input [3:0] A,
    input [3:0] B,
    input Cin,
    output reg [7:0] S,
    output reg Cout
    );    
    wire [4:0] C;
    wire [3:0] Q;
    FullAdder FA_0 (.A(A[0]),.B(B[0]),.Cin(Cin),.S(Q[0]));
    CarryLookAhead(.Cin(Cin),.C(C),.A(A),.B(B));
    
    genvar i;
    generate
    for (i = 1; i < 4; i = i + 1)
    begin : FA_gen
    FullAdder FA (.A(A[i]),.B(B[i]),.Cin(C[i]),.S(Q[i]));
    end
    endgenerate
    
    always@*
    begin
    S={C[4],Q};
    
    end
    
endmodule
