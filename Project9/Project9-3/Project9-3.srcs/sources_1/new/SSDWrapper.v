`timescale 1ns / 1ps

module counter(input clk, output reg [1:0] OUT);
    always@(posedge clk) begin
    OUT = OUT +1;
    end endmodule
    

module SSDWrapper(
    input [15:0] binary,
    input CLK,
    output [7:0] Cathodes,
    output [3:0] Anodes
    );
    
    wire [3:0] MUX2SSDDEC;
    wire [1:0] Select;
    wire counterClk;
    
    CLKDIV kiloclock(.clk(CLK),.rst(rst),.clk_div(counterClk),.terminalcount(199999));
    counter counter(.clk(counterClk),.OUT(Select));
    Encoder2to4 SSDAnodes(.IN(Select),.OUT(Anodes));
    MUX4TO1 MUX(.IN3(binary[15:12]),.IN2(binary[11:8]),.IN1(binary[7:4]),.IN0(binary[3:0]),.OUT(MUX2SSDDEC),.sel(Select));
    
    ssdDecoder SSDDEC(.inCat(MUX2SSDDEC),.rst(rst),.cat(Cathodes));
    
endmodule
