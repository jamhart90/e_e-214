`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/17/2020 03:53:03 PM
// Design Name: 
// Module Name: testbench
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module testbench;

reg S;
reg R;
wire Q;
wire Qn;

    sr_latch sr_latch(
    .S(S),
    .R(R),
    .Q(Q),
    .Qn(Qn)
    );
    
 initial begin
    // Initialize Inputs
    S = 0;
    R = 0;

    // Add stimulus here
    #100 R=1;
    #100 R=0;
    #100 S=1;
    #100 S=0;
    #100 R=1;
    #100 R=0;
    #100 S=1;
    #100 R=1;
    #100 S=0;
    #100 R=0;
end   

endmodule
