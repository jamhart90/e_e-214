`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/26/2020 08:06:02 PM
// Design Name: 
// Module Name: PISOShiftRegister
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module SIPOShiftRegister(
    input SDI,
    input CLK,
    input RST,
    output [7:0] Dout
    );
    wire [7:0] Q; // Outputs from D Flip Flops
    
    // Bused Mux to choose loading 
    
    DFF D0(.D(SDI),.CLK(CLK),.RST(RST),.Q(Q[0]));
    DFF D1(.D(Q[0]),.CLK(CLK),.RST(RST),.Q(Q[1]));
    DFF D2(.D(Q[1]),.CLK(CLK),.RST(RST),.Q(Q[2]));
    DFF D3(.D(Q[2]),.CLK(CLK),.RST(RST),.Q(Q[3]));
    DFF D4(.D(Q[3]),.CLK(CLK),.RST(RST),.Q(Q[4]));
    DFF D5(.D(Q[4]),.CLK(CLK),.RST(RST),.Q(Q[5]));
    DFF D6(.D(Q[5]),.CLK(CLK),.RST(RST),.Q(Q[6]));
    DFF D7(.D(Q[6]),.CLK(CLK),.RST(RST),.Q(Q[7]));
    assign Dout = Q;
endmodule
