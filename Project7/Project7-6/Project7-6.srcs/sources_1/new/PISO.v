`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/26/2020 08:06:02 PM
// Design Name: 
// Module Name: PISOShiftRegister
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module PISOShiftRegister(
    input SDI,
    input [7:0] Din,
    input CLK,
    input RST,
    input LE,
    output reg [7:0] Dout,
    output reg SDO
    );
    wire [7:0] Q; // Outputs from D Flip Flops
    reg [7:0] L; // Data to load into D Flip large
    
    // Bused Mux to choose loading 
    
    DFF D0(.D(L[0]),.CLK(CLK),.RST(RST),.Q(Q[0]));
    DFF D1(.D(L[1]),.CLK(CLK),.RST(RST),.Q(Q[1]));
    DFF D2(.D(L[2]),.CLK(CLK),.RST(RST),.Q(Q[2]));
    DFF D3(.D(L[3]),.CLK(CLK),.RST(RST),.Q(Q[3]));
    DFF D4(.D(L[4]),.CLK(CLK),.RST(RST),.Q(Q[4]));
    DFF D5(.D(L[5]),.CLK(CLK),.RST(RST),.Q(Q[5]));
    DFF D6(.D(L[6]),.CLK(CLK),.RST(RST),.Q(Q[6]));
    DFF D7(.D(L[7]),.CLK(CLK),.RST(RST),.Q(Q[7]));
    always @(posedge CLK)
    begin
    L = LE ? Din : {Q[6:0],SDI};
    Dout = Q;
    SDO = Q[7];
    end
endmodule
