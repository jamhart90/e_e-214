`timescale 1ns / 1ps

module wrapper(
    input SDI,
    input CLK,
    input RST,
    output [7:0] DataOut
    );
        
    SIPOShiftRegister SIPO_LO(.SDI(SDI),.CLK(CLK),.RST(RST),.Dout(DataOut[7:0]));    
endmodule
