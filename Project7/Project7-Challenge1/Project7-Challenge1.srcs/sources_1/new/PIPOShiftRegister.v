`timescale 1ns / 1ps

module PIPOShiftRegister(
    input [7:0] DataIn,
    input Clk,
	input Rst,
    output [7:0] DataOut
    );
    wire [7:0] latchout;    
    DFF d0( .D(DataIn[0]), .CLK(Clk), .RST(Rst), .Q(latchout[0]) );        
    DFF d1( .D(DataIn[1]), .CLK(Clk), .RST(Rst), .Q(latchout[1]) );        
    DFF d2( .D(DataIn[2]), .CLK(Clk), .RST(Rst), .Q(latchout[2]) );        
    DFF d3( .D(DataIn[3]), .CLK(Clk), .RST(Rst), .Q(latchout[3]) );        
    DFF d4( .D(DataIn[4]), .CLK(Clk), .RST(Rst), .Q(latchout[4]) );    
    DFF d5( .D(DataIn[5]), .CLK(Clk), .RST(Rst), .Q(latchout[5]) );
    DFF d6( .D(DataIn[6]), .CLK(Clk), .RST(Rst), .Q(latchout[6]) );
    DFF d7( .D(DataIn[7]), .CLK(Clk), .RST(Rst), .Q(latchout[7]) );
    assign DataOut = latchout;
endmodule
