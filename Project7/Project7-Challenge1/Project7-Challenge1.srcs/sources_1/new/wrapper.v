`timescale 1ns / 1ps

module wrapper(
    input [7:0] DataIn,     //
    input CLK,              //
    input RST,              //
    input FILL,             //
    input DIR,              
    input ROT,
    output [7:0] DataOut    // 
    );
    wire [7:0] loadedData;
    
    PIPOShiftRegister(.DataIn(DataIn), .Clk(CLK),.Rst(RST),.DataOut(loadedData));
    BarrelShifter(.DataIn(loadedData),.DataOut(DataOut),.Fill(FILL),.ShiftAmt(DataIn[3:0]),.Dir(DIR),.Rot(ROT));
endmodule
