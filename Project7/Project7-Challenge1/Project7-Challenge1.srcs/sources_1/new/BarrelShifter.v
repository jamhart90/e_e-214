`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/30/2020 10:59:31 PM
// Design Name: 
// Module Name: BarrelShifter
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module BarrelShifter(
    input [7:0] DataIn,
    output reg [7:0] DataOut,
    input Fill,
    input [3:0] ShiftAmt,
    input Dir,
    input Rot
    );
    reg [23:0] data;
    reg [23:0] shiftedData;
    always @*
    begin
    // selects between rotate and shift with fill bit
    data <= Rot ? {{8{Fill}},DataIn,{8{Fill}}} : {DataIn,DataIn,DataIn};
    
    // shift data left or right depending on value of Dir
    shiftedData <= Dir ? data >> ShiftAmt : data << ShiftAmt;
    
    // after shifting/rotating the data the middle 8 bits are output to DataOut
    DataOut <= shiftedData[15:8];
    end
    
endmodule
