`timescale 1ns / 1ps

module testbench;

reg D;
reg G;
wire Q;
wire Qn;

    d_latch d_latch(
    .D(D),
    .G(G),
    .Q(Q),
    .Qn(Qn)

    );
    
    initial
    begin
    
    D=0;
    G=0;
    
    #100 G=1;
    #100 G=0;
    #100 D=1;
    #100 D=0;
    #100
    #100 D=1;
    #100 G=1;
    #100 G=0;
    #100 D=0;
    #100 G=1;
    #100 D=1;
    #100 D=0;
    #100 G=0;
    
    end
endmodule
