`timescale 1ns / 1ps

module Mux(
    input [7:0] I0,
    input [7:0] I1,
    input Sel,
    output reg[7:0] O
    );
    
    always @(Sel,I1,I0)
    begin
    
    if(Sel) O = I1;
    else    O = I0;
    
    end
    
    
endmodule
