`timescale 1ns / 1ps

module d_latch(
input D,
input G,
output Q,
output Qn
    );
    
    wire S, R, Dn, Q_int, Qn_int;
    
assign #1 S     = ~(D & G);
assign #1 Dn    = ~D; 
assign #1 R     = ~(Dn & G); 
assign #1 Q_int    = ~(S & Qn_int);
assign #1 Qn_int   = ~(R & Q_int);
assign Q        = Q_int;
assign Qn       = Qn_int;   

endmodule