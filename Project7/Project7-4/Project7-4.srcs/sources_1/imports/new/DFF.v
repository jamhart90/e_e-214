`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/26/2020 08:07:21 PM
// Design Name: 
// Module Name: DFF
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module DFF(
    input D,
    input CLK,
    input RST,
    output reg Q
    );
    always @(posedge CLK,posedge RST)
    begin
    if(RST)
        Q <= 1'b0;
    else
        Q <= D;
    end
    
    
endmodule
