`timescale 1ns / 1ps

module DFF(
    input D,
    input CLK,
    input RST,
    output reg Q
    );
    always @(posedge CLK,posedge RST)
    begin
    if(RST)
        Q <= 1'b0;
    else
        Q <= D;
    end
    
    
endmodule
