`timescale 1ns / 1ps

module counter(
    input CLKin,
    input [4:0] divider,
    output reg Count
    );
    reg [32:0] counter;
    
    always @(posedge CLKin)
    begin
    counter = counter + 1;
    Count = counter[divider];
    end
    
endmodule

module wrapper(
    input [7:0] Din,
    input CLK,
    input RST,
    input [1:0] LE,
    output [15:0] DataOut
    
    );
    
    wire [1:0] SDOint;
    wire SlowClk;
    
    counter countchocolate(.CLKin(CLK),.Count(SlowClk),.divider(5'd22));
    
    PISOShiftRegister PISO_LO(.SDI(SDOint[1]),.Din(Din),.CLK(SlowClk),.RST(RST),.LE(LE[0]),.SDO(SDOint[0]),.Dout(DataOut[7:0]));
    
    PISOShiftRegister PISO_HI(.SDI(SDOint[0]),.Din(Din),.CLK(SlowClk),.RST(RST),.LE(LE[1]),.SDO(SDOint[1]),.Dout(DataOut[15:8]));
    
endmodule
