`timescale 1ns / 1ps

module encoder(
input enablein,
input [3:0] inputs,
output reg group,
output reg [1:0] outputs,
output reg enableout
    );
    
    always @ (inputs,enablein)
    begin
        if(enablein)
            outputs = (inputs[3] == 1) ? 2'b11 : (
                      (inputs[2] == 1) ? 2'b10 : (
                      (inputs[1] == 1) ? 2'b01 : 2'b00 ) );
        else
            outputs = 2'b00;
    end
    
    always @ (inputs,enablein)
    begin
        if (enablein ==1 && inputs == 0)
            enableout = 1;
        else
            enableout = 0;
    
    end
            
    always @ (inputs,enablein)
    begin
        if (enablein == 1 && inputs != 0)
            group = 1;
        else
            group = 0;
    end
    
endmodule
