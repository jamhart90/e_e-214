`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/02/2020 08:48:20 PM
// Design Name: 
// Module Name: testbench
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module testbench;
reg enablein = 0;
reg [3:0] inputs = 0;
wire group;
wire [1:0] outputs;
wire enableout;

    encoder uut(
    
    .enablein (enablein),
    .inputs (inputs),
    .group (group),
    .outputs (outputs),
    .enableout( enableout)
    );
    
    integer i = 0;
    integer j = 0;
    
    initial
    begin
    enablein= 0;
    inputs = 0;
    
    for (i = 0 ; i < 2 ; i=i+1)begin
    #10 enablein = i;
    for (j = 0 ; j < 16 ; j=j+1)
    #10 inputs = j;
   
   end
    #5 $finish;
    
    end
endmodule
