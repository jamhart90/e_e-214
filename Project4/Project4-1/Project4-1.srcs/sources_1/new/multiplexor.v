`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/30/2020 08:41:32 PM
// Design Name: 
// Module Name: multiplexor
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


 module multiplexor(
input [1:0] sel,
input [7:0] inputzer,
input [7:0] inputone,
input [7:0] inputtwo,
input [7:0] inputthr,
output [7:0] light
    );
    
    assign light =  (sel == 2'd0) ? inputzer : ( 
                    (sel == 2'd1) ? inputone : (
                    (sel == 2'd2) ? inputtwo : inputthr ) ); 
                    
endmodule
