`timescale 1ns / 1ps

// module definition of 4 to 1 multiplexor
module mux4to1(
    input [3:0] D,
    input [1:0] S,
    output reg Y
    );
    always @(D,S)
        Y = D[S];

endmodule

// module definition of 8 to 1 multiplexor
// using two 4 to 1 multiplexors
module mux8to1(
    input [7:0] D, // data bus in
    input [2:0] S, // data selector bus
    output Y       // data bit out
    );
    
    // wire for output of muxs
    wire [1:0]muxint;
    
    // mux for lowest 4 bits
mux4to1 muxlow(
    .D(D[3:0]),
    .S(S[1:0]),
    .Y(muxint[0])
    );
    
    // mux for highest 4 bits
mux4to1 muxhigh(
    .D(D[7:4]),
    .S(S[1:0]),
    .Y(muxint[1])
    );
    
    // mux for selecting between high or low mux
assign Y = muxint[S[2]];

endmodule
