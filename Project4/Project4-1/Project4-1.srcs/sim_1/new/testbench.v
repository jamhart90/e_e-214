`timescale 1ns / 1ps

module testbench;

reg [1:0] sel = 0;
reg [7:0] inputzer = 0;
reg [7:0] inputone = 0;
reg [7:0] inputtwo = 0;
reg [7:0] inputthr = 0;
wire [7:0] light;

multiplexor uut(

    .sel (sel),
    .inputzer (inputzer),
    .inputone (inputone),
    .inputtwo (inputtwo),
    .inputthr (inputthr),
    .light (light)
    );
    
    integer i = 0 ; 
    initial
    begin
    sel=0;
    inputzer = 3;
    inputone = 12;
    inputtwo = 48;
    inputthr = 192;
    for (i=0;i<4;i=i+1)
    #10 sel = i;
    #5 $finish;
    end
    
endmodule
