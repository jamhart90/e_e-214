`timescale 1ns / 1ps

module testbench;
reg [7:0] inputs;
reg [1:0] shifter;
reg dir;
reg rot;
reg fill;
wire [7:0] outputs;

shifter uut(
    .inputs (inputs),
    .shifter (shifter),
    .dir (dir),
    .rot (rot),
    .fill (fill),
    .outputs (outputs)
    );
    
    integer i = 0;
    integer j = 0;
    integer k = 0;
    integer l = 0;
    integer m = 0;
    initial
    begin
    inputs = 0;
    shifter = 0;
    dir = 0;
    rot = 0;
    fill = 0;
    for (i=0;i<2;i=i+1)begin
    #10 rot = i;
    for (j=0;j<2;j=j+1)begin
    #10 dir = j;
    for (k=0;k<4;k=k+1)begin
    #10 shifter = k;
    for (m=0;m<2;m=m+1) begin
    #10 fill =0;
    for (l=0;l<256;l=l+1) begin
    #10 inputs = l;
    end
    end
    end
    end
    end
    #5 $finish;
    end
endmodule
