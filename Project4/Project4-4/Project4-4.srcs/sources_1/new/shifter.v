`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/02/2020 10:28:29 PM
// Design Name: 
// Module Name: shifter
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module shifter(
input [7:0] inputs,
input [1:0] shifter,
input dir,
input rot,
input fill,
output reg [7:0] outputs
    );
    always @ (inputs)
    begin
            if (rot == 0)
                if (dir == 0)
                    outputs = (shifter == 0) ? inputs[7:0] : (
                              (shifter == 1) ? {inputs[6:0] , fill } : (
                              (shifter == 2) ? {inputs[5:0] , fill,fill } : {inputs[4:0],fill,fill,fill} ) );
                else
                    outputs = (shifter == 0) ? {inputs[7:0]} : (
                              (shifter == 1) ? {fill, inputs[7:1]} : (
                              (shifter == 2) ? {fill,fill, inputs[7:2]} : {fill,fill,fill, inputs[7:3]} ) );
            else
                if (dir == 0)
                    outputs = (shifter == 0) ? inputs[7:0] : (
                              (shifter == 1) ? {inputs[6:0] , inputs[7] } : (
                              (shifter == 2) ? {inputs[5:0] , inputs[7:6] } : {inputs[4:0],inputs[7:5]} ) );
                else
                    outputs = (shifter == 0) ? inputs[7:0] : (
                              (shifter == 1) ? {inputs[0], inputs[7:1]} : (
                              (shifter == 2) ? {inputs[1:0], inputs[7:2]} : {inputs[2:0], inputs[7:3]} ) );                 
     end                         
endmodule
