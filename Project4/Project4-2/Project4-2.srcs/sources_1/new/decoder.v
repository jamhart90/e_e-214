`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/01/2020 08:10:59 PM
// Design Name: 
// Module Name: decoder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module decoder(
input [1:0] i,
input [3:0] btn,
output reg [3:0] y
    );
    
    always @ (i)
    begin
        case(i)
            2'b00:
                y[0] = btn[0];
            2'b01:
                y[1] = btn[1];
            2'b10:
                y[2] = btn[2];
            2'b11:
                y[3] = btn[3];
            default:
                y = 0;
        endcase
    end
endmodule
