`timescale 1ns / 1ps

module Counter(
    input Cen,
    input clk,
    input rst,
    output [15:0] data
    );
    wire [3:0] tc;
    
    decimal_counter Thousanths(.Cen(Cen),.clk(clk),.rst(rst),.b(data[3:0]),.tc(tc[0]));
    decimal_counter Hundreths(.Cen(Cen&tc[0]),.clk(clk),.rst(rst),.b(data[7:4]),.tc(tc[1]));
    decimal_counter Tenths(.Cen(Cen&tc[0]&tc[1]),.clk(clk),.rst(rst),.b(data[11:8]),.tc(tc[2]));
    decimal_counter Seconds(.Cen(Cen&tc[0]&tc[1]&tc[2]),.clk(clk),.rst(rst),.b(data[15:12]),.tc(tc[3]));
    
endmodule
