`timescale 1ns / 1ps

module SegmentController(
    input enable,
    input [15:0] data,
    input [2:0] dp,
    input clk,
    input rst,
    output [3:0] anode,
    output [7:0] cathode
    );
    wire [1:0] sel;
    wire [3:0] segment;
    // 2 Counter
    TwoBitCounter Two_Counter(.clk(clk),.enable(enable),.b(sel));
    // 2:4 Decoder
    TwotoFourDecoder Two_to_Four_Decoder(.in(sel),.enable(enable),.anode(anode));
    // 4:1 Bus Mux
    FourtoOneMux Fout_to_One_Mux(.Dig0(data[3:0]),.Dig1(data[7:4]),.Dig2(data[11:8]),.Dig3(data[15:12]),.Sel(sel),.Digit(segment));
    // 7Seg Decoder
    SevenSegDecoder Seven_Segment_Decoder(.data_in(segment),.data_out(cathode[6:0]));
    // Dec Pnt Decoder
    DecimalPntDecoder Decimal_Point_Decoder(.decPlace(3'b100),.sel(sel),.dp(cathode[7]));
endmodule
