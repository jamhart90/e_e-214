`timescale 1ns / 1ps

module TwotoFourDecoder(
    input [1:0] in,
    input enable,
    output reg [3:0] anode
    );
    always@* begin
    if (enable) begin
        case(in)
            2'b00 : anode <= 4'b1110;
            2'b01 : anode <= 4'b1101;
            2'b10 : anode <= 4'b1011;
            2'b11 : anode <= 4'b0111;
        endcase
        end
    else
        anode <= 4'b1111;
    end 
endmodule
