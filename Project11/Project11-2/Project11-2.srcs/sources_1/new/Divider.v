`timescale 1ns / 1ps

module Divider(
    input rst,
    input clk_in,
    output reg clk_out
    );
    reg [15:0] count;
    wire tc;
    
    assign tc = (count == 49999);
    
    always@(posedge clk_in,posedge rst)begin
        if(rst) count <=0;
        else if (tc) count <=0;
        else count <= count +1;
    end
    
    always@(posedge clk_in,posedge rst) begin
        if (rst) clk_out <= 0;
        else if (tc) clk_out = ~clk_out;
    end     
endmodule
