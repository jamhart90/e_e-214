`timescale 1ns / 1ps

module decimal_counter(
    input Cen,
    input clk,
    input rst,
    output reg [3:0] b,
    output tc
    );
    always@(posedge clk, posedge rst) begin
    if (rst)
        b = 4'b0000;
    else if(Cen) begin
        if (b ==4'b1001) begin
            b = 4'b0000;
            end
        else begin
            b = b + 1;
        end
    end
    end
            
assign tc = (b == 4'b1001) ? 1:0;
endmodule
