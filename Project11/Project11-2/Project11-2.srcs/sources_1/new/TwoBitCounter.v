`timescale 1ns / 1ps

module TwoBitCounter(
    input clk,
    input enable,
    output reg [1:0] b
    );
    always@(posedge clk)begin
    if(enable)
        b <= b + 1;
    end
    
endmodule
