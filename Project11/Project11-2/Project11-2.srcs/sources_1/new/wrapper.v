`timescale 1ns / 1ps

module wrapper(
    input [3:0] btn,
    input clk,
    output [3:0] anode,
    output [7:0] cathode
    );
    wire CLK1KHz;
    wire Cen;
    wire [15:0] data;
    
    //Divider
    Divider Divider(.rst(btn[3]),.clk_in(clk),.clk_out(CLK1KHz));
    //State Machine
    StateMachine State_Machine(.start(btn[0]),.stop(btn[1]),.inc(btn[2]),.clk(CLK1KHz),.rst(btn[3]),.Cen(Cen));
    //Counter
    Counter Counter(.Cen(Cen),.clk(CLK1KHz),.rst(btn[3]),.data(data));
    //7Segment Controller
    SegmentController Seven_Seg_Controller(.enable(1'b1),.data(data),.dp(3'b111),.clk(CLK1KHz),.rst(btn[3]),.anode(anode),.cathode(cathode));
endmodule
