`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/15/2020 10:40:54 PM
// Design Name: 
// Module Name: DecimalPntDecoder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module DecimalPntDecoder(
    input [2:0] decPlace,
    input [1:0] sel,
    output reg dp
    );
    reg [3:0] dpdecode;
    always@* begin
        case(decPlace)
            3'b000 : dpdecode <= 4'b1111;
            3'b001 : dpdecode <= 4'b1110;
            3'b010 : dpdecode <= 4'b1101;
            3'b011 : dpdecode <= 4'b1011;
            3'b100 : dpdecode <= 4'b0111;
            default: dpdecode <= 4'b1111;
        endcase
    end
    always@(sel)begin    
        dp <= dpdecode[sel];
    end     
endmodule
