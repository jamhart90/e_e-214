`timescale 1ns / 1ps

module FourtoOneMux(
    input [3:0] Dig0,
    input [3:0] Dig1,
    input [3:0] Dig2,
    input [3:0] Dig3,
    input [1:0] Sel,
    output reg [3:0] Digit
    );
    always@* begin
    case(Sel) 
        2'b00 : Digit = Dig0;
        2'b01 : Digit = Dig1;
        2'b10 : Digit = Dig2;
        2'b11 : Digit = Dig3;
    endcase
    end
    
endmodule
