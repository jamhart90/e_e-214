`timescale 1ns / 1ps

module StateMachine(
    input start,
    input stop,
    input inc,
    input clk,
    input rst,
    output Cen
    );
    
    localparam S0 = 2'b00;
    localparam S1 = 2'b01;
    localparam S2 = 2'b10;
    localparam S3 = 2'b11;
    
    reg [1:0] pState,nState;
    
    always@(pState,start,stop,inc,rst) begin
        case(pState)
            S0 : nState = (start) ? S1 : (inc) ? S2 : S0;
            S1 : nState = (stop) ? S3 : S1;
            S2 : nState = (inc) ? S2 : S3;
            S3 : nState = (start) ? S1 : (inc) ? S2 : S3;
        endcase
    end
    
    always@(posedge clk,posedge rst) begin
        pState <= (rst) ? S0 : nState;
    end
    
    assign Cen = pState[0]^pState[1];
    
endmodule
