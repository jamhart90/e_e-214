`timescale 1ns / 1ps

module SerialAdder(
    input A,
    input B,
    input clk,
    input rst,
    output F,
    output Cout
    );
    localparam S0 = 2'b00;
    localparam S1 = 2'b01;
    localparam S2 = 2'b10;
    localparam S3 = 2'b11;
    
    reg [1:0] pState, nState;
    // Next State Logic
    always@(pState,A,B) begin
    case(pState)
        S0,S1 : nState = (A&B) ? S2 : (~A&~B) ? S0 : S1;
        S2,S3 : nState = (A&B) ? S3 : (~A&~B) ? S1 : S2;
        default: nState = S0;
    endcase
    end
    
    // State Registers
    always@(posedge(clk),posedge(rst))
    begin
        pState <= (rst) ? S0 : nState;
    end
    
    assign F = pState[0];
    assign Cout = pState[1];
endmodule
