`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/15/2020 09:01:14 PM
// Design Name: 
// Module Name: testBench
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module testBench();
    reg A;
    reg B;
    reg clk = 0;
    reg rst;
    wire F;
    wire Cout;
    
    
    always
        #10 clk = ~clk;
        
    SerialAdder SA(.A(A),.B(B),.clk(clk),.rst(rst),.F(F),.Cout(Cout));
    initial
    begin   
    A=0;
    B=0;    
    

    // initialize to S0
        #5 rst = 1;
        #10 rst = 0;
    // S0 to S0
        #10 A=0;
            B=0;
    // S0 to S1
        #10 A=1;
            B=0;
    // S1 to S1
        #25 A=0;
            B=1;
    // S1 to S0
        #10 A=0;
            B=0;
    // S0 to S2
        #10 A=1;
            B=1;
    // S2 to S2;
        #10 A=1;
            B=0;
    // S2 to S1;
        #10 A=0;
            B=0;
    // S1 to S2
        #10 A=1;
            B=1;   
    // S2 to S3
        #10 A=1;
            B=1;
    // S3 to S3
        #10;
    // S3 to S2;
        #10 A=0;
            B=1;
    // S2 to S3
        #10 A=1;
            B=1;
    // S3 to S1
        #10 A=0;
            B=0;
    // Reset
        #10 rst=1;
            A=0;
            B=0;
        #10 rst=0;
    end
endmodule
