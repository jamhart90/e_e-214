`timescale 1ns / 1ps

module CounterBasedClockDivider(
    input clk,
    input rst,
    output led
    );

wire counterWire;

ClkDivider onehundMtoonekilo(.clk(clk),.rst(rst),.clk_div(counterWire),.terminalcount(49999));
ClkDivider onekilotoonehz(.clk(counterWire),.rst(rst),.clk_div(led),.terminalcount(499));

endmodule