`timescale 1ns / 1ps

module tb;

    reg CEN = 0;
    reg CLK;
    wire TC;
    wire [3:0] B = 4'b0000;
    
    BCD BCD(.CEN(CEN),.CLK(CLK),.TC(TC),.B(B));
        integer i;  

    initial begin
    for (i = 0 ; i < 255 ; i = i + 1)    begin
          #10  CEN = 1;
          #10  CEN = 0;
      end
    
end

endmodule