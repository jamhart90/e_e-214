`timescale 1ns / 1ps

module twobitcounter(
    input CLK,
    output reg [1:0] OUT
    );
    
    always@(posedge CLK) begin
    OUT = OUT + 1;
    end
    
endmodule
