`timescale 1ns / 1ps

module counterfourU(
    input clk,
    output reg [3:0] out
    );
    always@(posedge clk)
    begin
    
    if ( out == 4'b1001)
        out = 4'b0;
    else
        out = out+1;
    end
endmodule

module mux(
input [3:0] IN0,
input [3:0] IN1,
input [3:0] IN2,
input [3:0] IN3,
input [1:0] SEL,
output reg [3:0] OUT);
always@(SEL) begin
case (SEL)
    2'b00: OUT = IN0;
    2'b01: OUT = IN1;
    2'b10: OUT = IN2;
    2'b11: OUT = IN3;
    endcase
    end
endmodule

module CounterBasedClockDivider(
    input clk,
    input rst,
    output [7:0] led,
    output [3:0] SegmentAnode
    );

wire counterWire;
<<<<<<< HEAD
wire OneHzout;
wire tenCount;
wire hundredCount;
wire thousandCount;
reg [3:0] digitsEnable;

always@(posedge OneHzout)
begin
digitsEnable[0] = OneHzout;
digitsEnable[1] = OneHzout & tenCount;
digitsEnable[2] = OneHzout & tenCount& hundredCount;
digitsEnable[3] = OneHzout & tenCount& hundredCount & thousandCount;
end

ClkDivider onehundMtoonekilo(.clk(clk),.rst(rst),.clk_div(counterWire),.terminalcount(49999));
ClkDivider onekilotoonehz(.clk(counterWire),.rst(rst),.clk_div(oneHzout),.terminalcount(499));


ssdDecoder(.clk(digitsEnable[0]),.cat(led));
ssdDecoder(.clk(digitsEnable[1]),.cat(led));
ssdDecoder(.clk(digitsEnable[2]),.cat(led));
ssdDecoder(.clk(digitsEnable[3]),.cat(led));


=======
wire oneHzout;
wire [3:0] ssdwire;
ClkDivider onehundMtoonekilo(.clk(clk),.rst(rst),.clk_div(counterWire),.terminalcount(49999));
ClkDivider onekilotoonehz(.clk(counterWire),.rst(rst),.clk_div(oneHzout),.terminalcount(499));
counterfourU(.clk(oneHzout),.out(ssdwire));
ssdDecoder(.inCat(ssdwire),.cat(led));
>>>>>>> 8-4Fix
assign SegmentAnode = 4'b1110;

endmodule