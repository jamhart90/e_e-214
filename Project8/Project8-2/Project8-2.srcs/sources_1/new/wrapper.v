`timescale 1ns / 1ps
module counter(
    input clk,
    output [3:0] counter
    );
reg [3:0] count;
always @(posedge clk)
    count = count +1;
    
    assign counter = count;   
endmodule


module clk_divider(
    input clk,
    input rst,
    output [3:0] led
    );

wire [26:0] din;
wire [26:0] clkdiv;

counter(.clk(clkdiv[26]),.counter(led));

FDC dff_inst0 (
    .C(clk),
    .CLR(rst),
    .D(din[0]),
    .Q(clkdiv[0])
);

genvar i;
generate
for (i = 1; i < 27; i=i+1)
begin : dff_gen_label
    FDC dff_inst (
        .C(clkdiv[i-1]),
        .CLR(rst),
        .D(din[i]),
        .Q(clkdiv[i])
    );
    end
endgenerate

assign din = ~clkdiv;

endmodule