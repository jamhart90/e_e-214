`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/23/2020 09:18:36 PM
// Design Name: 
// Module Name: fivewaylight
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module fivewaylight(
input [5:0] sw,
output light
    );
    // using xor for every switch only turns on the light when an odd number of switches are turn on
    // 00001 odd 
    // 11011 even
    //   E|0 0 0 0 1 1 1 1
    //   D|0 0 1 1 1 1 0 0
    //   C|0 1 1 0 0 1 1 0
    // A B|---------------
    // 0 0|0 1 0 1 0 1 0 1
    // 0 1|1 0 1 0 1 0 1 0
    // 1 1|0 1 0 1 0 1 0 1
    // 1 0|1 0 1 0 1 0 1 0
    // since no groups are identified and this pattern emulates the XOR map
        assign light = sw[0]^sw[1]^sw[2]^sw[3]^sw[4];   
endmodule
