`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/24/2020 09:31:10 PM
// Design Name: 
// Module Name: oddnumberdetector
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module oddnumberdetector(
input [7:0] sw,
input [3:0] btn,
output [1:0] led
    );
    assign led[0] = sw[0]^sw[1]^sw[2]^sw[3]^sw[4]^sw[5]^sw[6]^sw[7];
    assign led[1] = led[0] & ~( btn[0]^btn[1]^btn[2]^btn[3]);
endmodule
