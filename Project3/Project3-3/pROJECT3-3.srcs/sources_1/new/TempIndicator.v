`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/24/2020 07:45:13 PM
// Design Name: 
// Module Name: TempIndicator
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module TempIndicator(
input [7:0] sw,
output light
    );
    /*
    0�      = 00000000
    100.0�  = 11111111
    100/256 = 0.390325� / bit
    62.5�   = 160   = 10100000
    62.9�   = 161   = 10100001
    ...
    71.9�   = 184   = 10111000
    72.3�   = 185   = 10111001
    
        H0110011001100110
        G0011110000111100
        F0000111111110000
    ABCDE0000000011111111
    ---------------------
    0000|0000000000000000
    0001|0000000000000000
    0011|0000000000000000
    0010|0000000000000000
    0110|0000000000000000
    0111|0000000000000000
    0101|0000000000000000
    0100|0000000000000000
    1100|0000000000000000
    1101|0000000000000000
    1111|0000000000000000
    1110|0000000000000000
    1010|1111111111111111   
    1011|1111111100000011
    1001|0000000000000000
    1000|0000000000000000
    
    A and ~B and C and ~D or
    A and ~B and C and ~E or 
    A and ~B and C and ~F and ~G
    
    */
    assign light = (sw[7] & ~sw[6] & sw[5] & ~sw[4]) | (sw[7] & ~sw[6] & sw[5] & ~sw[3]) | (sw[7] & ~sw[6] & sw[5] & ~sw[2] & ~sw[1]);
endmodule
