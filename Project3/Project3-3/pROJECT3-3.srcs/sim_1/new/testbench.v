`timescale 1ns / 1ps

module testbench;

reg [7:0] sw = 0;
wire light;

    TempIndicator uut(
    .sw(sw),
    .light(light)
    );
    
    integer k = 0;
    initial
    begin
    sw=0;
    for (k=0;k<256;k=k+1)
    #10 sw=k;
    
    #5 $finish;
    end
    
endmodule
