`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/23/2020 11:22:23 PM
// Design Name: 
// Module Name: majorfive
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module testbench;

reg [4:0] sw = 0;
wire light;
    
    majorfive uut(
    
    .sw (sw),
    .light(light)
    );
    
    integer k = 0;
    initial
    begin
    sw = 0;
    for (k=0;k<32;k=k+1)
    #10 sw = k;
    
    #5 $finish;
    end
endmodule
