`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/16/2020 12:25:19 AM
// Design Name: 
// Module Name: wrapper
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module wrapper(
input [7:0] sw,
input btn,
input clk,
output wire [7:0] SegmentCathode,
output reg [3:0] SegmentAnode
);

wire [3:0] segSel;
wire Y;

mux segmentSelect(
    .onesDigit(sw[3:0]),
    .tensDigit(sw[7:4]),
    .sel(Y),
    .O(segSel)
    );    
    
ssdDecoder bin2seg(
    .inCat(segSel),
    .cat(SegmentCathode)
    );

counter counter(
    .clk(clk),
    .O(Y)
    );
    
always @(Y)
begin
if (Y)
    SegmentAnode = 4'b1101;
else
    SegmentAnode = 4'b1110;
end
    
endmodule
