`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/16/2020 07:35:58 PM
// Design Name: 
// Module Name: mux
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mux(
input [3:0] onesDigit,
input [3:0] tensDigit,
input sel,
output reg [3:0] O
    );
always @(sel,onesDigit,tensDigit)
begin
if (sel)
    O <= tensDigit;
else
    O <= onesDigit;
end
endmodule









