`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/16/2020 12:25:19 AM
// Design Name: 
// Module Name: wrapper
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module wrapper(
input [3:0] inCat,
output wire [7:0] OutCat,
output wire [3:0] OutAn
    );
    
    
ssdDecoder bin2seg(
    .inCat(inCat),
    .cat(OutCat)
    );
    
    assign OutAn = 4'b1110;
    
endmodule
