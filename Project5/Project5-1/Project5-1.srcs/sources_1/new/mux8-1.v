module mux(
input [7:0] I,
input [2:0] Sel,
output reg O
);

always @(Sel,I)
begin
    O = I[Sel];        
end
endmodule