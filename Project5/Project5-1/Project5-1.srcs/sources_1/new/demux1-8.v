module demux(
    input I,
    input [2:0] Sel,
    output reg [7:0] O,
    output reg Indicator
);

always @(Sel,I)
begin
    if (I)
    begin
        Indicator = I;
        O = 8'b00000001 << Sel;
    end
    else
    begin
        Indicator = 0;
        O = 8'b00000000;
    end
end
endmodule