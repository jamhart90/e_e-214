`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/13/2020 09:15:48 PM
// Design Name: 
// Module Name: wrapper
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module wrapper(
input [7:0] I,
input clk,
output wire [7:0] O,
output wire Indicator
    );

wire [2:0] Sel;
wire data;

mux input_mux(
    .I(I),
    .Sel(Sel),
    .O(data)
);

demux output_mux(
    .I(data),
    .Sel(Sel),
    .O(O),
    .Indicator(Indicator)
);

counter counter(
    .clk(clk),
    .O(Sel)
);
endmodule
