`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/18/2020 01:44:23 AM
// Design Name: 
// Module Name: CreateANewCircuit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module CreateANewCircuit(
input [3:0] sw,
output [2:0] led
    );
    // Red Light
    assign led[0] = ( sw[0]& sw[1]&~sw[2]&~sw[3])| // 3
                    ( sw[0]& sw[1]& sw[2]&~sw[3])| // 7
                    ( sw[0]& sw[1]&~sw[2]& sw[3])| // 11
                    (~sw[0]&~sw[1]& sw[2]& sw[3])| // 12
                    ( sw[0]&~sw[1]& sw[2]& sw[3])| // 13
                    (~sw[0]& sw[1]& sw[2]& sw[3])| // 14
                    ( sw[0]& sw[1]& sw[2]& sw[3]); // 15
                    
    // Yellow Light
    assign led[1] = (~sw[0]& sw[1]&~sw[2]&~sw[3])| // 2
                    (~sw[0]&~sw[1]& sw[2]&~sw[3])| // 4
                    ( sw[0]&~sw[1]& sw[2]&~sw[3])| // 5
                    (~sw[0]& sw[1]& sw[2]&~sw[3])| // 6
                    ( sw[0]& sw[1]& sw[2]&~sw[3])| // 7
                    (~sw[0]&~sw[1]&~sw[2]& sw[3])| // 8
                    ( sw[0]&~sw[1]&~sw[2]& sw[3])| // 9
                    ( sw[0]& sw[1]&~sw[2]& sw[3])| // 11
                    ( sw[0]&~sw[1]& sw[2]& sw[3])| // 13
                    ( sw[0]& sw[1]& sw[2]& sw[3]); // 15
                    
    assign led[2] = (~sw[0]& sw[1]&~sw[2]&~sw[3])| // 2
                    (~sw[0]&~sw[1]& sw[2]&~sw[3])| // 4
                    ( sw[0]&~sw[1]& sw[2]&~sw[3])| // 5
                    (~sw[0]& sw[1]& sw[2]&~sw[3])| // 6
                    ( sw[0]& sw[1]& sw[2]&~sw[3])| // 7
                    (~sw[0]&~sw[1]&~sw[2]& sw[3])| // 8
                    ( sw[0]&~sw[1]&~sw[2]& sw[3])| // 9
                    ( sw[0]& sw[1]&~sw[2]& sw[3])| // 11
                    ( sw[0]&~sw[1]& sw[2]& sw[3])| // 13
                    ( sw[0]& sw[1]& sw[2]& sw[3]); // 15
                    
endmodule
