`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/17/2020 08:38:23 PM
// Design Name: 
// Module Name: duplicatecircuits
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module duplicatecircuits(
input [7:0] sw,
output [3:0] led
    );
    // s s
    // w w | 0 0 1 1 sw[4]
    // 6 5 | 0 1 1 0 sw[3]
    // -------------
    // 0 0 | 0 1 1 0 
    // 0 1 | 1 0 0 1
    // 1 1 | 1 0 0 1 
    // 1 0 | 0 1 1 0
    // 
    assign led[3] = (~sw[3]&sw[5])|(sw[3]&~sw[5]);
endmodule
