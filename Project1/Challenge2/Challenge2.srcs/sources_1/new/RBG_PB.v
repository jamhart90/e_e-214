`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/09/2020 11:31:21 PM
// Design Name: 
// Module Name: RBG_PB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module RBG_PB(
input [2:0] btn,
input sw,
output reg [2:0] rgb
    );
    always @ (sw)
    if (sw)rgb <= btn;
    else rgb <= 0;
endmodule
