REM
REM Vivado(TM)
REM htr.txt: a Vivado-generated description of how-to-repeat the
REM          the basic steps of a run.  Note that runme.bat/sh needs
REM          to be invoked for Vivado to track run status.
REM Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
REM

vivado -log RBG_PB.vdi -applog -m64 -product Vivado -messageDb vivado.pb -mode batch -source RBG_PB.tcl -notrace
