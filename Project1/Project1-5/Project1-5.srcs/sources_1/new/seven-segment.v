`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/09/2020 10:13:22 PM
// Design Name: 
// Module Name: seven-segment
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module sevensegment(
output [7:0] seg,
input [7:0] sw,
input [3:0] btn,
output [3:0] an
    );
    assign an = ~btn;
    assign seg = ~sw;
endmodule
