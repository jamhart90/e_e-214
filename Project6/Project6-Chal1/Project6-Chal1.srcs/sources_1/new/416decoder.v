`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/16/2020 11:48:34 PM
// Design Name: 
// Module Name: 416decoder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module four16decoder(
input [3:0] I,
output reg [15:0] O
    );
always @(I)
    begin
        O = 16'b0000000000000001 << I;
    end
endmodule
