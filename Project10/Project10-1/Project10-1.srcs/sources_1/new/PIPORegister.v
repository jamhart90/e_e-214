`timescale 1ns / 1ps
module DFF(
    input D,
    input LoadEnable,
    input RST,
    output reg Q
    );
    always @(posedge LoadEnable,posedge RST)
    begin
    if(RST)
        Q <= 1'b0;
    else
        Q <= D;
    end
    
    
endmodule

module PIPORegister #(parameter WIDTH = 8) (
    input [WIDTH-1:0] DataIn,
    input LoadEnable,
	input Rst,
    output [WIDTH-1:0] DataOut
    );
    generate
    genvar i;
    for(i = 0 ; i < WIDTH ; i = i+1) begin : DFF
        DFF DFF(.D(DataIn[i]),.LoadEnable(LoadEnable),.RST(Rst),.Q(DataOut[i]));
        end
    endgenerate
       
endmodule
