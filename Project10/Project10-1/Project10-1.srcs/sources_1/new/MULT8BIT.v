`timescale 1ns / 1ps

module MULT8BIT(
    input [7:0] A,
    input [7:0] B,
    output [15:0] X
    );
    assign X = A*B;
endmodule
