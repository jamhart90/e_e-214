`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/15/2020 01:49:21 PM
// Design Name: 
// Module Name: ALU
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ALU(
    input [7:0] A,
    input [7:0] B,
    input [2:0] OP,
    output reg [3:0] Flags,
    output reg [8:0] F
    );
    always@(OP)
    begin
    case(OP)
        3'b000 : F = A+B;
        3'b001 : F = A+8'b00000001;
        3'b010 : F = A-B;
        3'b011 : F = A^B;
        3'b100 : F = A|B;
        3'b101 : F = A&B;
        default: F = 8'b0;
    endcase
    Flags = {F[7]^F[8],F[8],F[7],F==0};
//    // Zero Flag
//    if (F == 8'b00000000)
//        Flags[0] = 1'b1;   
//    // Negative Flag
//    if (F[7])
//        Flags[1] = 1'b1;
//    // Carry Flag    
//    if (F[8])
//        Flags[2] = 1'b1;
//    // Overflow Flag    
//    if (F[7]^F[8])
//        Flags[3] = 1'b1;
    end    
    
endmodule
