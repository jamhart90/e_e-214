`timescale 1ns / 1ps

module wrapper(
    input [7:0] SW,
    input [2:0] OP,
    input [1:0] LE,
    input Reset,
    input clk,
    output [7:0] Cathodes,
    output [3:0] Anodes,
    output [3:0] Flags
    );
    wire [7:0] A;
    wire [7:0] B;
    wire [15:0] S;
    
    PIPORegister AReg(.DataIn(SW),.LoadEnable(LE[0]),.Rst(Reset),.DataOut(A));
    PIPORegister BReg(.DataIn(SW),.LoadEnable(LE[1]),.Rst(Reset),.DataOut(B));
    
    ALU ALU(.A(A),.B(B),.OP(OP),.F(S),.Flags(Flags));
    
    SSDWrapper DisplayControl(.binary(S),.CLK(clk),.Cathodes(Cathodes),.Anodes(Anodes));
    
    
endmodule
