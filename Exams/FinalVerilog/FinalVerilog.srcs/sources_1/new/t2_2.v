module t2_2 (input [7:0] A, B, [1:0] Sel, output Z, [7:0]  F);
reg F;
always @(Sel) begin
case (Sel)
    2'b00 : F = A + B; //  Add Operation
    2'b01 : F = A >> 1;// Shift right 1 bit
    2'b10 : F = A & B;// bitwise and
    2'b11 : F = A ^ B;// bitwise xor
    endcase
end
assign Z = (F == 8'b00000000) ? 1 :0;
endmodule