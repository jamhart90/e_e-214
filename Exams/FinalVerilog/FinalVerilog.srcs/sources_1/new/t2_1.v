module t2_1(input clk, rst, A, [7:0] D, output X, Y);
reg [15:0] counter = 0;
always @(posedge clk) begin
    counter = A ? counter + 1 : counter;
end
 assign X = counter < D;
 assign Y = counter > D;
endmodule
