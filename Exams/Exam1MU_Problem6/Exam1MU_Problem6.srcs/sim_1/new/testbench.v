`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/02/2020 07:23:43 PM
// Design Name: 
// Module Name: testbench
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module testbench;

reg [7:0] IN;
reg clk;
wire [7:0] OUT;

wrapper wrapper(.IN(IN),.clk(clk),.OUT(OUT));

    integer k = 0;
    initial
    begin
    clk = 0;
    IN = 8'b01011010;
    
    for (k=0;k<128;k=k+1)
    #10 clk = k;
    
    #5 $finish;
    end
endmodule
