`timescale 1ns / 1ps


module mux(input [7:0] IN,input [2:0] SEL, output reg OUT);
    always@*
    OUT = IN[SEL];
endmodule

module encoder(input IN,input[2:0] SEL,output reg [7:0] OUT);
    always @(SEL)
    case(SEL)
        3'd0:   OUT[0] = IN;
        3'd1:   OUT[1] = IN;
        3'd2:   OUT[2] = IN;
        3'd3:   OUT[3] = IN;
        3'd4:   OUT[4] = IN;
        3'd5:   OUT[5] = IN;
        3'd6:   OUT[6] = IN;
        3'd7:   OUT[7] = IN;
    endcase
endmodule

module counter(input clk,output reg [2:0] out);
    reg [31:0] counter;
    always @(posedge clk)
    begin
    counter = counter +1;  
    out = counter[2:0];
    end
endmodule
    
module wrapper(input [7:0] IN, input clk,output [7:0] OUT);
    wire [2:0] SEL;
    wire data;
    
    mux mux1(.IN(IN),.SEL(SEL),.OUT(data));
    encoder enc1(.IN(data),.SEL(SEL),.OUT(OUT));
    counter clock(.clk(clk),.out(SEL));
endmodule
