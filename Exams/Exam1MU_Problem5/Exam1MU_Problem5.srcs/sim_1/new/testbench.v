`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/02/2020 06:53:10 PM
// Design Name: 
// Module Name: testbench
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module testbench;

    reg [4:0] IN;
    wire OUT;
    
    wrapper wrapper(.IN(IN),.OUT(OUT));
    
    integer k = 0 ;
    initial
    begin
    IN = 0;
    for (k=0;k<16;k=k+1)
    #10 IN = k;
    
    #10 $finish;
    end
endmodule
