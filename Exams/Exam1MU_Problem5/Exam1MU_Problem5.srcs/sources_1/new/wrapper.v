`timescale 1ns / 1ps

module wrapper(
    input [4:0] IN,
    output reg OUT
    );
    wire [3:0] muxin;
    // sigma m (3,5,6,7,10,12,14)
    // A B C D Y
    // 0 0 0 0 0 A = 0
    // 0 0 0 1 0 B = 0
    // 0 0 1 0 0 Y = C & D
    // 0 0 1 1 1
    // -------------------
    // 0 1 0 0 0 A = 0
    // 0 1 0 1 1 B = 1
    // 0 1 1 0 1 Y = C | D
    // 0 1 1 1 1
    // -------------------
    // 1 0 0 0 0 A = 1
    // 1 0 0 1 0 B = 0
    // 1 0 1 0 1 Y = C & ~D
    // 1 0 1 1 0
    // -------------------
    // 1 1 0 0 1 A = 1
    // 1 1 0 1 0 B = 1
    // 1 1 1 0 1 Y = ~D
    // 1 1 1 1 0   
    
    always @*
    begin
    case (IN[3:2]) // A = IN[3], b = IN[2]
        2'b00: OUT = IN[1] & IN[0];
        2'b01: OUT = IN[1] | IN[0];
        2'b10: OUT = IN[1] & ~IN[0];
        2'b11: OUT = ~IN[0];
    endcase    
    end
endmodule
